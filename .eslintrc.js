module.exports = {
  globals: {
    defineProps: 'readonly',
    defineEmits: 'readonly',
    defineExpose: 'readonly',
    withDefaults: 'readonly'
  },
  parser: 'vue-eslint-parser',
  env: {
    browser: true,
    node: true,
    es2021: true,
    'vue/setup-compiler-macros': true
  },
  extends: [
    'plugin:vue/vue3-essential',
    'plugin:prettier/recommended',
    'plugin:vue/vue3-recommended',
    '@vue/typescript/recommended'
  ],
  parserOptions: {
    ecmaVersion: 12,
    sourceType: 'module'
  },
  rules: {
    'prettier/prettier': 'error',
    'vue/first-attribute-linebreak': [
      2,
      {
        // 单行时，第一属性前不允许使用换行符
        singleline: 'beside',
        // 多行时，第一属性前必须使用换行符
        multiline: 'below'
      }
    ],
    // 强制每行的最大属性数
    'vue/max-attributes-per-line': [
      2,
      {
        // 单行时可以接收最大数量
        singleline: 10,
        // 多行时可以接收最大数量
        multiline: {
          max: 1
        }
      }
    ],
    'vue/singleline-html-element-content-newline': 'off',
    'vue/html-self-closing': 'off',
    semi: ['error', 'always']
  },
  ignorePatterns: ['auto-imports.d.ts', 'components.d.ts']
};
