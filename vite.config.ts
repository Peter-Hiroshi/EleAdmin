import { defineConfig } from 'vite';
import vue from '@vitejs/plugin-vue';
import { viteMockServe } from 'vite-plugin-mock';
// import components on demand
import Components from 'unplugin-vue-components/vite';
// Auto import APIs on-demand for Vite
import AutoImport from 'unplugin-auto-import/vite';
import { ElementPlusResolver } from 'unplugin-vue-components/resolvers';

// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-ignore
import Icons from 'unplugin-icons/vite';
// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-ignore
import IconsResolver from 'unplugin-icons/resolver';

// https://vitejs.dev/config/
// noinspection JSUnusedGlobalSymbols
export default defineConfig({
  plugins: [
    vue(),
    AutoImport({
      resolvers: [ElementPlusResolver(), IconsResolver({})]
    }),
    Components({
      resolvers: [ElementPlusResolver(), IconsResolver({})],
      directoryAsNamespace: true
    }),
    Icons({
      autoInstall: true
    }),
    viteMockServe({ supportTs: true })
  ],
  css: {
    // preprocessorOptions: {
    //   css: {
    //     additionalData: `@import "src/assets/styles/index.css";`
    //   }
    // }
  }
});
