import { MockMethod } from 'vite-plugin-mock';

import { Curd, Table, TableResult } from "../src/components/table/table";
import { Form } from '../src/components/form/form';
import { Toolbar } from '../src/components/button/button';
import { View } from '../src/components/core';

function getRows(number: number, index = 0) {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  const result: any[] = [];
  for (let i = 0; i < number; i++) {
    result.push({
      id: `${index + i}`,
      date: "@date('yyyy-MM-dd')",
      address: '@city()',
      name: '@cname()'
      // avatar: Random.image('400x400', Random.color(), Random.color(), Random.first()),
      // imgArr: getRandomPics(Math.ceil(Math.random() * 3) + 1),
      // imgs: getRandomPics(Math.ceil(Math.random() * 3) + 1),
      // date: '@datetime',
      // time: `@time('HH:mm')`,
      // 'no|100000-10000000': 100000,
      // 'status|1': ['normal', 'enable', 'disable'],
    });
  }

  return result;
}

const table: Table = {
  label: '表格',
  name: 'table',
  selector: true,
  columns: [
    {
      name: 'name',
      label: '姓名',
      width: 200
    },
    {
      name: 'date',
      label: '出生日期',
      face: 'success',
      cell: 'tag',
      width: 300
    },
    {
      name: 'address',
      label: '家庭住址'
    }
  ]
};

const search: Form = {
  name: 'search',
  label: '查询',
  fields: [
    {
      name: 'name',
      label: '姓名',
      widget: 'input',
      col: 6
    },
    {
      name: 'sex',
      label: '性别',
      widget: 'radio',
      options: [
        {
          label: '男',
          value: 1
        },
        {
          label: '女',
          value: 0
        }
      ],
      col: 6
    }
  ]
};

const toolbar: Toolbar = {
  items: [
    { command: {}, name: 'edit', icon: 'IEpEdit' },
    { command: {}, name: 'add', icon: 'IEpDelete' }
  ]
};

export default [
  {
    url: '/api/table/index',
    method: 'get',
    response: () => {
      return {
        value: {
          title: '页面表格',
          view: 'Curd',
          description: '这是一段表格介绍',
          ...table,
          search,
          toolbar: toolbar.items,
          embedment: toolbar.items,
          url: 'api/table/rows'
        } as View & Curd
      };
    }
  },
  {
    url: '/api/table/rows',
    method: 'get',
    response: () => {
      return {
        value: {
          rows: getRows(10),
          pagination: {
            'total|50-300': 80,
            'current|0-5': 3
          }
        } as TableResult
      };
    }
  }
] as MockMethod[];
