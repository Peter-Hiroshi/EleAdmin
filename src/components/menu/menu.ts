import { Er, ID } from '../elementUI';
import { CommandOptions } from '../core';

export interface Menu extends Er {
  [ID]: string | number;
  items?: Array<Menu>;
  command?: CommandOptions;
}
